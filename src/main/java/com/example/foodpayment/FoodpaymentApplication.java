package com.example.foodpayment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FoodpaymentApplication {

	public static void main(String[] args) {
		SpringApplication.run(FoodpaymentApplication.class, args);
	}

}
