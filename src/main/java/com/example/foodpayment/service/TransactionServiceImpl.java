package com.example.foodpayment.service;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.foodpayment.dto.PaymentRequest;
import com.example.foodpayment.dto.ResponseDto;
import com.example.foodpayment.entity.Transaction;
import com.example.foodpayment.repository.TransactionRepository;

/**
 * @author rakeshr.ra
 *
 */
@Service
public class TransactionServiceImpl implements TransactionService {
	@Autowired
	TransactionRepository transactionRepository;

	@Override
	public ResponseDto makePayments(PaymentRequest paymentRequest) {

		Transaction transaction = new Transaction();
		transaction.setAccountNumber(paymentRequest.getUserAccountNumber());
		transaction.setAmount(paymentRequest.getAmount());
		transaction.setComments("debit");
		transaction.setTransaction_date(LocalDateTime.now());
		Transaction transaction2 = new Transaction();
		transaction2.setAccountNumber(paymentRequest.getVendorAccountNumber());
		transaction2.setAmount(paymentRequest.getAmount());
		transaction2.setComments("credit");
		transaction2.setTransaction_date(LocalDateTime.now());
		transactionRepository.save(transaction);
		transactionRepository.save(transaction2);

		return new ResponseDto(4004, "transaction success");
	}

}
