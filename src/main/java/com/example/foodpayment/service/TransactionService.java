package com.example.foodpayment.service;

import org.springframework.stereotype.Service;

import com.example.foodpayment.dto.PaymentRequest;
import com.example.foodpayment.dto.ResponseDto;

/**
 * @author rakeshr.ra
 *
 */
@Service
public interface TransactionService {

	ResponseDto makePayments(PaymentRequest paymentRequest);
}
