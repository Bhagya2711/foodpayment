package com.example.foodpayment.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentRequest {

	private Long orderNumber;
	private Long userAccountNumber;
	private Long vendorAccountNumber;
	private double amount;

}
