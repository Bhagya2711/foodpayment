package com.example.foodpayment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.foodpayment.dto.PaymentRequest;
import com.example.foodpayment.dto.ResponseDto;
import com.example.foodpayment.service.TransactionService;

@RequestMapping
@RestController
public class TransactionController {
	@Autowired
	TransactionService transactionService;

	@PostMapping("/payments")
	public ResponseEntity<ResponseDto> makePayments(@RequestBody PaymentRequest paymentRequest) {

		return new ResponseEntity<>(transactionService.makePayments(paymentRequest), HttpStatus.OK);

	}

}
