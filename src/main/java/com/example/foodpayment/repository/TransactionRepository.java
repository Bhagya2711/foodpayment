package com.example.foodpayment.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.foodpayment.entity.Transaction;

/**
 * @author rakeshr.ra
 *
 */
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Long> {

}
